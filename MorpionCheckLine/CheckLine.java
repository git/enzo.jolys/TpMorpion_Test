package MorpionCheckLine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Morpion.Game;

class CheckLine {

	@Test
	void CheckLineDiagonaleJ1(){
		
		Game game = new Game();
		
		int tab[][] = {{1,0,0},
				  	{0,1,0},
				  	{0,0,1}};
		
		assertEquals(1,game.checkLine(tab));
	}
	
	@Test
	void CheckLineDiagonaleJ2(){
		
		Game game = new Game();
		
		int tab[][] = {{2,0,0},
				  	{0,2,0},
				  	{0,0,2}};
		
		assertEquals(2,game.checkLine(tab));
	}
	
	@Test
	void CheckLineHorizontaleJ1(){
		
		Game game = new Game();
		
		int tab[][] = {{1,1,1},
				  	{0,0,0},
				  	{0,0,0}};
		
		assertEquals(1,game.checkLine(tab));
	}

	@Test
	void CheckLineHorizontaleJ2(){
		
		Game game = new Game();
		
		int tab[][] = {{2,2,2},
				  	{0,0,0},
				  	{0,0,0}};
		
		assertEquals(2,game.checkLine(tab));
	}
	
	@Test
	void CheckLineVerticaleJ1(){
		
		Game game = new Game();
		
		int tab[][] = {{1,0,0},
				  	{1,0,0},
				  	{1,0,0}};
		
		assertEquals(1,game.checkLine(tab));
	}
	
	@Test
	void CheckLineVerticaleJ2(){
		
		Game game = new Game();
		
		int tab[][] = {{2,0,0},
				  	{2,0,0},
				  	{2,0,0}};
		
		assertEquals(2,game.checkLine(tab));
	}
}
