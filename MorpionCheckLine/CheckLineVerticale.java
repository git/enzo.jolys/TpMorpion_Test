package MorpionCheckLine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Morpion.Game;

class CheckLineVerticale {

	@Test
	void CheckLineVerticale0Test() {
		Game game = new Game();
		
		int[][] matrice = {{1,0,0},
						  {1,0,0},
						  {1,0,0}};
		assertEquals(1,game.checkLineVerticale(matrice));
	}
	
	@Test
	void CheckLineVerticale1Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,1,0},
						  {0,1,0},
						  {0,1,0}};
		assertEquals(1,game.checkLineVerticale(matrice));
	}
	
	@Test
	void CheckLineVerticale2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,1},
						  {0,0,1},
						  {0,0,1}};
		assertEquals(1,game.checkLineVerticale(matrice));
	}
	
	@Test
	void CheckLineVerticale0J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{2,0,0},
						  {2,0,0},
						  {2,0,0}};
		assertEquals(2,game.checkLineVerticale(matrice));
	}
	
	@Test
	void CheckLineVerticale1J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,2,0},
						  {0,2,0},
						  {0,2,0}};
		assertEquals(2,game.checkLineVerticale(matrice));
	}
	
	@Test
	void CheckLineVerticale2J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,2},
						  {0,0,2},
						  {0,0,2}};
		assertEquals(2,game.checkLineVerticale(matrice));
	}
}
