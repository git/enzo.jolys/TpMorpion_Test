package Morpion;

public class Game {
	
	public int[][] tab = {{0,0,0},
			  		{0,0,0},
			  		{0,0,0}};
	
	public void setTab(int[][] tab) {
		this.tab = tab;
	}
	
	public int checkLineHorizontale(int[][] matrice) {
		if ( matrice[0][0] == 1
				&& matrice[0][1] == 1
				&& matrice[0][2] == 1){
			return 1;
		}
		else if ( matrice[1][0] == 1
				&& matrice[1][1] == 1
				&& matrice[1][2] == 1){
			return 1;
		}
		else if ( matrice[2][0] == 1
				&& matrice[2][1] == 1
				&& matrice[2][2] == 1){
			return 1;
		}
		else if ( matrice[0][0] == 2
				&& matrice[0][1] == 2
				&& matrice[0][2] == 2){
			return 2;
		}
		else if ( matrice[1][0] == 2
				&& matrice[1][1] == 2
				&& matrice[1][2] == 2){
			return 2;
		}
		else if ( matrice[2][0] == 2
				&& matrice[2][1] == 2
				&& matrice[2][2] == 2){
			return 2;
		}
		return 0 ;
	}
	
	
	public int checkLineVerticale(int[][] matrice) {
		if ( matrice[0][0] == 1
				&& matrice[1][0] == 1
				&& matrice[2][0] == 1) {
			return 1;
		}
		else if ( matrice[0][1] == 1
				&& matrice[1][1] == 1
				&& matrice[2][1] == 1) {
			return 1;
		}
		else if ( matrice[0][2] == 1
				&& matrice[1][2] == 1
				&& matrice[2][2] == 1) {
			return 1;
		}
		else if ( matrice[0][0] == 2
				&& matrice[1][0] == 2
				&& matrice[2][0] == 2) {
			return 2;
		}
		else if ( matrice[0][1] == 2
				&& matrice[1][1] == 2
				&& matrice[2][1] == 2) {
			return 2;
		}
		else if ( matrice[0][2] == 2
				&& matrice[1][2] == 2
				&& matrice[2][2] == 2) {
			return 2;
		}
		return 0;
	}
	
	
	public int checkLineDiagonale(int[][] matrice) {
		
		if ( matrice[0][0] == 1
				&& matrice[1][1] == 1
				&& matrice[2][2] == 1) {
			return 1;
		}
		else if ( matrice[0][2] == 1
				&& matrice[1][1] == 1
				&& matrice[2][0] == 1) {
			return 1;
		}
		else if ( matrice[0][0] == 2
				&& matrice[1][1] == 2
				&& matrice[2][2] == 2) {
			return 2;
		}
		else if ( matrice[0][2] == 2
				&& matrice[1][1] == 2
				&& matrice[2][0] == 2) {
			return 2;
		}
		return 0;
	}
	
	
	public Boolean insertion(int player,int x,int y) {
		
		if ( (x > 2) || (y > 2 ) || x < 0 || y < 0) {
			return false;
		}
		if ( player != 1 && player != 2) {
			return false;
		}
		if ( tab[y][x] != 0 ) {
			return false;
		}
		tab[y][x] = player;
		return true;
	}
	
	public int checkLine(int[][] tableau) {
		
		int result = checkLineDiagonale(tableau);
		if ( result == 1 || result == 2 ) {
			return result;
		}
		result = checkLineVerticale(tableau);
		if ( result == 1 || result == 2 ) {
			return result;
		}
		result = checkLineHorizontale(tableau);
		if ( result == 1 || result == 2 ) {
			return result;
		}
		return 0;
	}
	
	public void affichageTab(){
		System.out.println("-------------")
		for( int i = 0 ; i < 3 ; i++){
			System.out.println("|");
			for( int a = 0 ; i < 3 ; a++){
				System.out.println(tab[i][a]+"|");
			}
			System.out.println("|");
		}
		System.out.println("-------------");
	}

	
	public void start() {
		int x,y,playerWin,player = 1 ;
		Scanner scan = new Scanner(System.in)

		System.out.println("Game start !!");
		while( playerWin = checkLine(tab) == 0 ){
			if ( player == 2 ){
				player--;
			}

			System.out.println("Joueur 1 :");
			System.out.println("Ecriver les coordonnées que vous voulez jouer !");
			System.out.println("X (compris entre 1 et 3) : ");
			x = scan.nextInt();
			System.out.println("Y (compris entre 1 et 3) : ");
			y = scan.nextInt();
			if ( insertion(player,x,y)){
				System.out.println("Coup valide !");
			}else {
				System.out.println("Ce coup n'est pas valide !");
			}
			player++;
		}

		if ( playerWin == 1 ){
			System.out.println("Le joueur 1 à gagné félicitation !!");
		} else {
			System.out.println("Le joueur 2 à gagné félicitation !!");
		}
		System.out.println("Game end !!");
	}
}
	
