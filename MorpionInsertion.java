package Morpion;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Morpion.Game;

class MorpionInsertion {

	@Test
	void InsertionPieceJ1() {
	
		Game game = new Game();
		
		int player = 1;
		
		assertTrue(game.insertion(player,0,0));
		assertTrue(game.insertion(player,0,1));
		assertTrue(game.insertion(player,1,0));
		assertTrue(game.insertion(player,1,1));
		assertTrue(game.insertion(player,1,2));
		assertTrue(game.insertion(player,2,1));
		assertTrue(game.insertion(player,2,2));
		assertTrue(game.insertion(player,2,0));
		assertTrue(game.insertion(player,0,2));
		
		assertFalse(game.insertion(player,-5,1));
		
		assertFalse(game.insertion(5,0,0));
		
		assertFalse(game.insertion(player,-9,-1));
		
		game.tab[2][2] = 2 ;
		assertFalse(game.insertion(player,2,2));
	}
	
	
	@Test
	void InsertionPieceJ2() {
	
		Game game = new Game();
		
		int player = 2;
		
		assertTrue(game.insertion(player,0,0));
		assertTrue(game.insertion(player,0,1));
		assertTrue(game.insertion(player,1,0));
		assertTrue(game.insertion(player,1,1));
		assertTrue(game.insertion(player,1,2));
		assertTrue(game.insertion(player,2,1));
		assertTrue(game.insertion(player,2,2));
		assertTrue(game.insertion(player,2,0));
		assertTrue(game.insertion(player,0,2));
		
		assertFalse(game.insertion(player,-5,1));
		
		assertFalse(game.insertion(5,0,0));
		
		assertFalse(game.insertion(player,-9,-1));
		
		game.tab[2][2] = 2 ;
		assertFalse(game.insertion(player,2,2));
	}
}
