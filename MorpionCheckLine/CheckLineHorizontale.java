package MorpionCheckLine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Morpion.Game;

class CheckLineHorizontale {

	@Test
	void CheckLineHorizontale0J1Test() {
		Game game = new Game();
		
		int[][] matrice = {{1,1,1},
						  {0,0,0},
						  {0,0,0}};
		assertEquals(1,game.checkLineHorizontale(matrice));
	}
	
	@Test
	void CheckLineHorizontale0J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{2,2,2},
						  {0,0,0},
						  {0,0,0}};
		assertEquals(2,game.checkLineHorizontale(matrice));
	}
	
	@Test
	void CheckLineHorizontale1J1Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,0},
						  {1,1,1},
						  {0,0,0}};
		assertEquals(1,game.checkLineHorizontale(matrice));
	}
	
	@Test
	void CheckLineHorizontale1J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,0},
						  {2,2,2},
						  {0,0,0}};
		assertEquals(2,game.checkLineHorizontale(matrice));
	}
	
	@Test
	void CheckLineHorizontale2J1Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,0},
						  {0,0,0},
						  {1,1,1}};
		assertEquals(1,game.checkLineHorizontale(matrice));
	}
	
	@Test
	void CheckLineHorizontale2J2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,0},
						  {0,0,0},
						  {2,2,2}};
		assertEquals(2,game.checkLineHorizontale(matrice));
	}

}
