package MorpionCheckLine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Morpion.Game;

class CheckLineDiagonale {

	@Test
	void CheckLineDiagonaleHGBDTest() {
		Game game = new Game();
		
		int[][] matrice = {{1,0,0},
						  {0,1,0},
						  {0,0,1}};
		assertEquals(1,game.checkLineDiagonale(matrice));
	}
	
	@Test
	void CheckLineDiagonaleHDBGTest() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,1},
						  {0,1,0},
						  {1,0,0}};
		assertEquals(1,game.checkLineDiagonale(matrice));
	}
	
	@Test
	void CheckLineDiagonaleHGBDJ2Test() {
		Game game = new Game();
		
		int[][] matrice = {{2,0,0},
						  {0,2,0},
						  {0,0,2}};
		assertEquals(2,game.checkLineDiagonale(matrice));
	}
	
	@Test
	void CheckLineDiagonaleHDBGJ2Test() {
		Game game = new Game();
		
		int[][] matrice = {{0,0,2},
						  {0,2,0},
						  {2,0,0}};
		assertEquals(2,game.checkLineDiagonale(matrice));
	}

}
